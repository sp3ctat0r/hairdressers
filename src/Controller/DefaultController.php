<?php

namespace App\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Routing\Annotation\Route;

class DefaultController extends Controller
{
  /**
  * @Route("/", name="home")
  * @Template()
  */
  public function home()
  {
    $em = $this->getDoctrine()->getManager();

    $contentBlock1 = $em->getRepository('App:Content')->findBy(
      ['page' => 'home', 'block' => 'home_block_1'],
      ['id' => 'DESC'],
      1
    );

    $contentBlock2 = $em->getRepository('App:Content')->findBy(
      ['page' => 'home', 'block' => 'home_block_2'],
      ['id' => 'DESC'],
      1
    );

    $content = [
      'home_block_1' => $contentBlock1[0] ?? null,
      'home_block_2' => $contentBlock2[0] ?? null
    ];

    return [
      'content' => $content
    ];
  }

  /**
   * @Route("/about", name="about")
   * @Template()
   */
  public function about()
  {
    return [

    ];
  }

  /**
   * @Route("/blog", name="blog")
   * @Template()
   */
  public function blog()
  {
    return [

    ];
  }

  /**
   * @Route("/contact", name="contact")
   * @Template()
   */
  public function contact()
  {
    return [

    ];
  }
}