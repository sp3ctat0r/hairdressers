<?php

namespace App\Controller;

use App\Entity\Content;
use App\Form\ContentType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

/**
 * @Route("/admin")
 */
class AdminController extends Controller
{
  /**
   * @Route("/", name="admin")
   * @Method({"GET"})
   * @Template()
   */
  public function admin()
  {
    return [];
  }

  /**
   * @Route("/clear_cache", name="clear_cache")
   * @Method({"GET"})
   * @Template()
   */
  public function clearCache()
  {
    $adminPage = $this->generateUrl('admin');

    $files = glob($this->container->getParameter('kernel.root_dir') . '/../var/cache/*');

    foreach ($files as $file) {
      $this->rrmdir($file);
    }

    header('Location: ' . $adminPage);
    exit;
  }

  /**
   * @param $dir
   */
  private function rrmdir($dir) {
    if (is_dir($dir)) {
      $objects = scandir($dir);
      foreach ($objects as $object) {
        if ($object != "." && $object != "..") {
          if (is_dir($dir."/".$object))
            $this->rrmdir($dir."/".$object);
          else
            unlink($dir."/".$object);
        }
      }
      rmdir($dir);
    }
  }

  /**
   * @Route("/upload_image", name="upload_image")
   * @Method({"GET","POST"})
   * @Template()
   */
  public function uploadImage(Request $request)
  {
    $form = $this
      ->createFormBuilder()
      ->add('file', FileType::class)
      ->add('upload', SubmitType::class)
      ->getForm()
    ;

    $publicDir = $this->container->getParameter('kernel.root_dir') . '/../public/images/';

    $images = scandir($publicDir);

    $form->handleRequest($request);

    if ($form->isSubmitted() && $form->isValid()) {

      $file = $form->get('file')->getData();

      $fileName = $this->generateUniqueFileName() . '.' . $file->guessExtension();

      $file->move($publicDir, $fileName);

      return $this->redirectToRoute('upload_image');
    }

    foreach ($images as $key => $image) {
      if ($image == '.' || $image == '..') {
        unset($images[$key]);
      }
    }

    return [
      'images' => $images,
      'form' => $form->createView()
    ];
  }

  /**
   * @Route("/remove_image/{name}", name="remove_image")
   * @Method("GET")
   * @Template()
   */
  public function removeImage(Request $request, $name)
  {
    $filename = $this->container->getParameter('kernel.root_dir') . '/../public/images/' . $name;

    if (file_exists($filename)) {
      unlink($filename);
    } else {
      exit;
      $this->addFlash('warning', 'File doesn\'t exist');
    }

    return $this->redirectToRoute('upload_image');
  }

  /**
   * @return string
   */
  private function generateUniqueFileName()
  {
    return md5(uniqid());
  }

  /**
   * @Route("/content/{page}/{block}/{offset}", name="content")
   * @Method({"POST", "GET"})
   */
  public function content(Request $request, $page, $block, $offset = 0)
  {
    $em = $this->getDoctrine()->getManager();

    if ($request->getMethod() == 'GET') {

      $content = $em->getRepository('App:Content')->findBy(
        ['block' => $block, 'page' => $page ],
        ['id' => 'DESC'],
        1,
        $offset
      );

      $content = $content[0] ?? new Content();

    } else {
      $content = new Content();
    }

    $content->setBlock($block);
    $content->setPage($page);

    $form = $this->createForm(ContentType::class, $content);

    $form->handleRequest($request);

    if ($form->isSubmitted() && $form->isValid()) {
      $em = $this->getDoctrine()->getManager();
      $em->persist($content);
      $em->flush();

      return $this->redirectToRoute('home');
    }

    return $this->render('default/' . $page . '.html.twig', array(
      'form' => $form->createView(),
    ));
  }
}
