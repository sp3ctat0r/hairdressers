<?php

namespace App\Form;

use App\Entity\Content;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;


class ContentType extends AbstractType
{
  /**
   * {@inheritdoc}
   */
  public function buildForm(FormBuilderInterface $builder, array $options)
  {
    // By default, form fields include the 'required' attribute, which enables
    // the client-side form validation. This means that you can't test the
    // server-side validation errors from the browser. To temporarily disable
    // this validation, set the 'required' attribute to 'false':
    // $builder->add('content', null, ['required' => false]);
    $builder
      ->add('title', TextareaType::class)
      ->add('content', TextareaType::class)
      ->add('save', SubmitType::class)
    ;
  }
  /**
   * {@inheritdoc}
   */
  public function configureOptions(OptionsResolver $resolver)
  {
    $resolver->setDefaults([
      'data_class' => Content::class,
    ]);
  }
}