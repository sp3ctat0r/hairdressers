<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity
 * @ORM\Table(name="content")
 */
class Content
{
  public static $content_blocks = [
    'home' => [
      'home_block_1',
      'home_block_2',
    ],
  ];

  /**
   * @var int
   *
   * @ORM\Id
   * @ORM\GeneratedValue
   * @ORM\Column(type="integer")
   */
  private $id;

  /**
   * @var string
   *
   * @ORM\Column(type="text")
   *
   */
  private $block;

  /**
   * @var string
   *
   * @ORM\Column(type="text")
   */
  private $page;

  /**
   * @var string
   *
   * @ORM\Column(type="text")
   * @Assert\Length(
   *     max=100,
   *     maxMessage="comment.too_long"
   * )
   */
  private $title;

  /**
   * @var string
   *
   * @ORM\Column(type="text")
   * @Assert\Length(
   *     max=10000,
   *     maxMessage="comment.too_long"
   * )
   */
  private $content;

  /**
   * @return int
   */
  public function getId()
  {
    return $this->id;
  }

  /**
   * @param int $id
   * @return Content
   */
  public function setId(int $id)
  {
    $this->id = $id;
    return $this;
  }

  /**
   * @return string
   */
  public function getBlock()
  {
    return $this->block;
  }

  /**
   * @param string $block
   * @return Content
   */
  public function setBlock(string $block)
  {
    $this->block = $block;
    return $this;
  }

  /**
   * @return string
   */
  public function getPage()
  {
    return $this->page;
  }

  /**
   * @param string $page
   * @return Content
   */
  public function setPage(string $page)
  {
    $this->page = $page;
    return $this;
  }

  /**
   * @return string
   */
  public function getTitle()
  {
    return $this->title;
  }

  /**
   * @param string $title
   * @return Content
   */
  public function setTitle(string $title)
  {
    $this->title = $title;
    return $this;
  }

  /**
   * @return string
   */
  public function getContent()
  {
    return $this->content;
  }

  /**
   * @param string $content
   * @return Content
   */
  public function setContent(string $content)
  {
    $this->content = $content;
    return $this;
  }


}